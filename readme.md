# E-Lami
---
### Installation Process
**Open Git bash**

Step 1: run `git clone https://nishantnepali@bitbucket.org/nishantnepali/e-lami.git"
---
Step 2 : run `cd e-lami`
---
step 3 : Rename `.env.example` to `.env`
---
step 4 : run `composer update` to update your project
---
step 5 : (optional) run `php artisan key:generate` to generate application key
---
step 6 : run `php artisan serve` to open the lite server of your local machine - it will open default port of :8000
---
step 7 : test application on `localhost:8000`

### For Further Information about Laravel
go to [Laravel]('https://laravel.com') official documentation